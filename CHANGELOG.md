
# Changelog for Social Networking Library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v4.0.0]

- removed deps for social networking library
- added social service client dep

## [v3.7.0] - 2023-03-30

- update gcube dependencies lower bound range. See #24836
- removed unused dependencies: common-uri, common-util--encryption
- fix portal-auth-library range #24943

## [v3.6.4] - 2022-06-15

- removed home-library

## [v3.6.3] - 2021-06-24

- Added common-gcube-calls lib 

## [v3.6.2] - 2021-05-20

Added IAM and event to orchestrator libs

## [v3.6.1] - 2021-03-25

Updated accounting libs version

## [v1.0.0] - 2013-01-11

First release
